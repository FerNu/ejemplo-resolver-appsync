## Hacer deploy del Lambda desde gitlab

1.  **Crear función lambda.**

-   Elegir Author from Scratch

-   Escribir un nombre para la función

-   Runtime: Node.js 18.x

-   Arquitectura: x86_64

-   Acceder a un usuario con permisos de lambda y crear access keys.

2.  **Configurar el repositorio**

Para Gitlab :

-   Ir a Settings -\> CI/CD-\>variables

-   Agregar variable

-   Crear variable AWS_ACCESS_KEY_ID y darle el valor de Access key

-   Crear variable AWS_SECRET_ACCESS_KEY y darle el valor de secret Access key

-   Asegurarse de que ambas variables esten secretas y enmascaradas

-   Editar el archivo **.gitlab-ci.yml**

-   Cambiar la región por la del lambda creado

-   Cambiar el nombre por el del lambda creado

-   Al realizar el commit se debería actualizar el código de la función
    lambda.

3.  **Agregar nueva query a appsync**

-   Ingresar a la API GraphQL

-   Si no existe, agregar la funcion lambda como data source.

-   Modificar schema de la api, se van a agregar en este caso un tipo nuevo (el tipo respuesta), que contiene un atributo llamado "mensaje" y es un string.

Agregar al schema:
```
type Respuesta {
    mensaje : String
}

```
-   Luego se agrega la query get_mensaje, al tipo Query ya existente, esta funcion va a retornar un mensaje tipo respuesta. 
```

type Query {
    get_mensaje : Respuesta
    ...
    ...
}
schema {
    query : Query
}
```

-   A la derecha, buscar la query y hacer click en el botón Attach para
    agregar un resolver a la query

-   Elegir opción Pipeline resolver como tipo y AppSync JavaScript como
    runtime.
    
-   Seleccionar la funcion que tenga como data source el lambda que fue creado, si esta funcion no existe agregarla de esta forma:.

    -   Elegir la data source que se acaba de crear

    -   Darle un nombre a la función del resolver

    -   Agregar el siguiente codigo a la función:


    ``` 
    export function request(ctx) {

    return {

    \"version\": \"2018-05-29\",

    \"operation\": \"Invoke\",

    \"payload\": {}

    };

    }
    export function response(ctx) {

    return ctx.result

    }

    ```

En este caso el atributo payload va vacio ya que no se envia ningun parametro a la funcion lambda.

-   Agregar función al resolver


4.  **Probar query**

-   Ir a la opción queries en el menú.

-   Elegir la query "get_mensaje" que se acaba de crear.

-   Hacer click en "Run", debería mostrar un mensaje de "hola mundo".
